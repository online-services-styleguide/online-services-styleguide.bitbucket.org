/*!
 * jQuery Placeholder Plugin v2.3.1
 * https://github.com/mathiasbynens/jquery-placeholder
 *
 * Copyright 2011, 2015 Mathias Bynens
 * Released under the MIT license
 */
(function(factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function($) {

    /****
     * Allows plugin behavior simulation in modern browsers for easier debugging. 
     * When setting to true, use attribute "placeholder-x" rather than the usual "placeholder" in your inputs/textareas 
     * i.e. <input type="text" placeholder-x="my placeholder text" />
     */
    var debugMode = false; 

    // Opera Mini v7 doesn't support placeholder although its DOM seems to indicate so
    var isOperaMini = Object.prototype.toString.call(window.operamini) === '[object OperaMini]';
    var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini && !debugMode;
    var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini && !debugMode;
    var valHooks = $.valHooks;
    var propHooks = $.propHooks;
    var hooks;
    var placeholder;
    var settings = {};

    if (isInputSupported && isTextareaSupported) {

        placeholder = $.fn.placeholder = function() {
            return this;
        };

        placeholder.input = true;
        placeholder.textarea = true;

    } else {

        placeholder = $.fn.placeholder = function(options) {

            var defaults = {customClass: 'placeholder'};
            settings = $.extend({}, defaults, options);

            return this.filter((isInputSupported ? 'textarea' : ':input') + '[' + (debugMode ? 'placeholder-x' : 'placeholder') + ']')
                .not('.'+settings.customClass)
                .not(':radio, :checkbox, [type=hidden]')
                .bind({
                    'focus.placeholder': clearPlaceholder,
                    'blur.placeholder': setPlaceholder
                })
                .data('placeholder-enabled', true)
                .trigger('blur.placeholder');
        };

        placeholder.input = isInputSupported;
        placeholder.textarea = isTextareaSupported;

        hooks = {
            'get': function(element) {

                var $element = $(element);
                var $passwordInput = $element.data('placeholder-password');

                if ($passwordInput) {
                    return $passwordInput[0].value;
                }

                return $element.data('placeholder-enabled') && $element.hasClass(settings.customClass) ? '' : element.value;
            },
            'set': function(element, value) {

                var $element = $(element);
                var $replacement;
                var $passwordInput;

                if (value !== '') {

                    $replacement = $element.data('placeholder-textinput');
                    $passwordInput = $element.data('placeholder-password');

                    if ($replacement) {
                        clearPlaceholder.call($replacement[0], true, value) || (element.value = value);
                        $replacement[0].value = value;

                    } else if ($passwordInput) {
                        clearPlaceholder.call(element, true, value) || ($passwordInput[0].value = value);
                        element.value = value;
                    }
                }

                if (!$element.data('placeholder-enabled')) {
                    element.value = value;
                    return $element;
                }

                if (value === '') {
                    
                    element.value = value;
                    
                    // Setting the placeholder causes problems if the element continues to have focus.
                    if (element != safeActiveElement()) {
                        // We can't use `triggerHandler` here because of dummy text/password inputs :(
                        setPlaceholder.call(element);
                    }

                } else {
                    
                    if ($element.hasClass(settings.customClass)) {
                        clearPlaceholder.call(element);
                    }

                    element.value = value;
                }
                // `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
                return $element;
            }
        };

        if (!isInputSupported) {
            valHooks.input = hooks;
            propHooks.value = hooks;
        }

        if (!isTextareaSupported) {
            valHooks.textarea = hooks;
            propHooks.value = hooks;
        }

        $(function() {
            // Look for forms
            $(document).delegate('form', 'submit.placeholder', function() {
                
                // Clear the placeholder values so they don't get submitted
                var $inputs = $('.'+settings.customClass, this).each(function() {
                    clearPlaceholder.call(this, true, '');
                });

                setTimeout(function() {
                    $inputs.each(setPlaceholder);
                }, 10);
            });
        });

        // Clear placeholder values upon page reload
        $(window).bind('beforeunload.placeholder', function() {

            var clearPlaceholders = true;

            try {
                // Prevent IE javascript:void(0) anchors from causing cleared values
                if (document.activeElement.toString() === 'javascript:void(0)') {
                    clearPlaceholders = false;
                }
            } catch (exception) { }

            if (clearPlaceholders) {
                $('.'+settings.customClass).each(function() {
                    this.value = '';
                });
            }
        });
    }

    function args(elem) {
        // Return an object of element attributes
        var newAttrs = {};
        var rinlinejQuery = /^jQuery\d+$/;

        $.each(elem.attributes, function(i, attr) {
            if (attr.specified && !rinlinejQuery.test(attr.name)) {
                newAttrs[attr.name] = attr.value;
            }
        });

        return newAttrs;
    }

    function clearPlaceholder(event, value) {
        
        var input = this;
        var $input = $(this);
        
        if (input.value === $input.attr((debugMode ? 'placeholder-x' : 'placeholder')) && $input.hasClass(settings.customClass)) {
            
            input.value = '';
            $input.removeClass(settings.customClass);

            if ($input.data('placeholder-password')) {

                $input = $input.hide().nextAll('input[type="password"]:first').show().attr('id', $input.removeAttr('id').data('placeholder-id'));
                
                // If `clearPlaceholder` was called from `$.valHooks.input.set`
                if (event === true) {
                    $input[0].value = value;

                    return value;
                }

                $input.focus();

            } else {
                input == safeActiveElement() && input.select();
            }
        }
    }

    function setPlaceholder(event) {
        var $replacement;
        var input = this;
        var $input = $(this);
        var id = input.id;

        // If the placeholder is activated, triggering blur event (`$input.trigger('blur')`) should do nothing.
        if (event && event.type === 'blur' && $input.hasClass(settings.customClass)) {
            return;
        }

        if (input.value === '') {
            if (input.type === 'password') {
                if (!$input.data('placeholder-textinput')) {
                    
                    try {
                        $replacement = $input.clone().prop({ 'type': 'text' });
                    } catch(e) {
                        $replacement = $('<input>').attr($.extend(args(this), { 'type': 'text' }));
                    }

                    $replacement
                        .removeAttr('name')
                        .data({
                            'placeholder-enabled': true,
                            'placeholder-password': $input,
                            'placeholder-id': id
                        })
                        .bind('focus.placeholder', clearPlaceholder);

                    $input
                        .data({
                            'placeholder-textinput': $replacement,
                            'placeholder-id': id
                        })
                        .before($replacement);
                }

                input.value = '';
                $input = $input.removeAttr('id').hide().prevAll('input[type="text"]:first').attr('id', $input.data('placeholder-id')).show();

            } else {
                
                var $passwordInput = $input.data('placeholder-password');

                if ($passwordInput) {
                    $passwordInput[0].value = '';
                    $input.attr('id', $input.data('placeholder-id')).show().nextAll('input[type="password"]:last').hide().removeAttr('id');
                }
            }

            $input.addClass(settings.customClass);
            $input[0].value = $input.attr((debugMode ? 'placeholder-x' : 'placeholder'));

        } else {
            $input.removeClass(settings.customClass);
        }
    }

    function safeActiveElement() {
        // Avoid IE9 `document.activeElement` of death
        try {
            return document.activeElement;
        } catch (exception) {}
    }
}));

/* no-js test */
(function() {
  document.documentElement.className = document.documentElement.className.replace('no-js', 'js');
})();

$(function() {
  /* Nav sm */
  (function() {
    function showNav() {
      var navHeight = (window.innerHeight ? window.innerHeight : $(window).height()) - 49;

      $('header.sm .nav-icon').addClass('open');
      $('body').css('overflow', 'hidden');
      $('.nav-sm-container')
        .css({
          width: $(window).width() + (!/*@cc_on!@*/0 ? 0 : 17),
          height: navHeight
        })
        .slideDown(250);
    }

    function hideNav() {
      $('header.sm .nav-icon').removeClass('open');
      $('body').css('overflow', 'auto');
      $('.nav-sm-container').slideUp(250).scrollTop(0);
    }

    $('header.sm .nav-icon').click(function() {
      $('body').scrollTop(0);

      if ($(this).hasClass('open')) {
        hideNav();
      } else {
        showNav();
      }
    });

    $(window).resize(function() {
      hideNav();
    });
  })();

  /* Tree */
  (function() {
    // init tree
    $('ul.tree li').prepend('<i></i>').has('ul').addClass('p');

    $('ul.tree li.p > i').click(function(e) {
      e.stopPropagation();
      $(this).parent('li').toggleClass('open').children('ul').slideToggle(250);

      if ($(this).siblings('a').attr('href') !== 'javascript:;') {
        $(this).siblings('a').toggleClass('ux-change');
      }
    });

    $('ul.tree li.p > a').click(function(e) {
      e.stopPropagation();
      e.preventDefault();

      if (! $(this).parent('li').hasClass('open')) {
        // open tree
        $(this).parent('li').toggleClass('open').children('ul').slideToggle(250);

        if ($(this).attr('href') !== 'javascript:;') {
          $(this).addClass('ux-change');
        }
      } else {
        if ($(this).attr('href') !== 'javascript:;') {
          // goto url
          window.location = $(this).attr('href');
        } else {
          // close tree
          $(this).parent('li').toggleClass('open').children('ul').slideToggle(250);
        }
      }
    });
  })();

  /* Fixed Header */
  (function() {
    var currentState = false;

    function fixedHeader() {
      var newState = null;

      if ($(this).scrollTop() > $('header.md > div:first-child').outerHeight()) {
        $('header.md').addClass('fixed');
        newState = true;
      } else {
        $('header.md').removeClass('fixed');
        newState = false;
      }

      if (currentState !== newState) {
        tapout();
        currentState = newState;
      }
    }

    $(window)
      .scroll(fixedHeader)
      .resize(fixedHeader)
      .trigger('scroll');
  })();

  /* Aux Nav Search */
  (function() {
    $('.search-form .search').click(function(e) {
      e.preventDefault();

      if (! $(this).parent('form').hasClass('search-form-active')) {
        // show form and focus
        $(this).parent('form').addClass('search-form-active');
        $(this).siblings('input').focus();
      } else {
        if ($(this).siblings('input').val()) {
          // submit form
          $(this).parent('form').submit();
        } else {
          // hide form
          $(this).parent('form').removeClass('search-form-active');
        }
      }
    });
  })();

  /* Main Nav */
  function tapout() {
    $('#main-nav > ul > li').removeClass('nav-hover');
  }

  (function() {
    $('body').on('click', function(e) {
      if (! $.contains($('#main-nav')[0], e.target)) {
        tapout();
      }
    });

    $('#main-nav > ul > li').on('click', function(e) {
      if (! $(this).hasClass('nav-hover')) {
        tapout();
        $(this).addClass('nav-hover');
      } else {
        tapout();
      }
    });
  })();

  /* Footer nav */
  (function() {
    $('footer h5').click(function() {
      if ($(this).css('cursor') === 'pointer') {
        $(this).toggleClass('open').next().slideToggle(250);
        $('body, html').animate({
          scrollTop: $(this).offset().top
        }, 600);
      }
    });
  })();

  /* HTML5 placeholder, ie */
  $('input, textarea').placeholder();
});
