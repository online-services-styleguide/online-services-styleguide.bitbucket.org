/* no-js test */
(function() {
  document.documentElement.className = document.documentElement.className.replace('no-js', 'js');
})();

$(function() {
  /* Nav sm */
  (function() {
    function showNav() {
      var navHeight = (window.innerHeight ? window.innerHeight : $(window).height()) - 49;

      $('header.sm .nav-icon').addClass('open');
      $('body').css('overflow', 'hidden');
      $('.nav-sm-container')
        .css({
          width: $(window).width() + (!/*@cc_on!@*/0 ? 0 : 17),
          height: navHeight
        })
        .slideDown(250);
    }

    function hideNav() {
      $('header.sm .nav-icon').removeClass('open');
      $('body').css('overflow', 'auto');
      $('.nav-sm-container').slideUp(250).scrollTop(0);
    }

    $('header.sm .nav-icon').click(function() {
      $('body').scrollTop(0);

      if ($(this).hasClass('open')) {
        hideNav();
      } else {
        showNav();
      }
    });

    $(window).resize(function() {
      hideNav();
    });
  })();

  /* Tree */
  (function() {
    // init tree
    $('ul.tree li').prepend('<i></i>').has('ul').addClass('p');

    $('ul.tree li.p > i').click(function(e) {
      e.stopPropagation();
      $(this).parent('li').toggleClass('open').children('ul').slideToggle(250);

      if ($(this).siblings('a').attr('href') !== 'javascript:;') {
        $(this).siblings('a').toggleClass('ux-change');
      }
    });

    $('ul.tree li.p > a').click(function(e) {
      e.stopPropagation();
      e.preventDefault();

      if (! $(this).parent('li').hasClass('open')) {
        // open tree
        $(this).parent('li').toggleClass('open').children('ul').slideToggle(250);

        if ($(this).attr('href') !== 'javascript:;') {
          $(this).addClass('ux-change');
        }
      } else {
        if ($(this).attr('href') !== 'javascript:;') {
          // goto url
          window.location = $(this).attr('href');
        } else {
          // close tree
          $(this).parent('li').toggleClass('open').children('ul').slideToggle(250);
        }
      }
    });
  })();

  /* Fixed Header */
  (function() {
    var currentState = false;

    function fixedHeader() {
      var newState = null;

      if ($(this).scrollTop() > $('header.md > div:first-child').outerHeight()) {
        $('header.md').addClass('fixed');
        newState = true;
      } else {
        $('header.md').removeClass('fixed');
        newState = false;
      }

      if (currentState !== newState) {
        tapout();
        currentState = newState;
      }
    }

    $(window)
      .scroll(fixedHeader)
      .resize(fixedHeader)
      .trigger('scroll');
  })();

  /* Aux Nav Search */
  (function() {
    $('.search-form .search').click(function(e) {
      e.preventDefault();

      if (! $(this).parent('form').hasClass('search-form-active')) {
        // show form and focus
        $(this).parent('form').addClass('search-form-active');
        $(this).siblings('input').focus();
      } else {
        if ($(this).siblings('input').val()) {
          // submit form
          $(this).parent('form').submit();
        } else {
          // hide form
          $(this).parent('form').removeClass('search-form-active');
        }
      }
    });
  })();

  /* Main Nav */
  function tapout() {
    $('#main-nav > ul > li').removeClass('nav-hover');
  }

  (function() {
    $('body').on('click', function(e) {
      if (! $.contains($('#main-nav')[0], e.target)) {
        tapout();
      }
    });

    $('#main-nav > ul > li').on('click', function(e) {
      if (! $(this).hasClass('nav-hover')) {
        tapout();
        $(this).addClass('nav-hover');
      } else {
        tapout();
      }
    });
  })();

  /* Footer nav */
  (function() {
    $('footer h5').click(function() {
      if ($(this).css('cursor') === 'pointer') {
        $(this).toggleClass('open').next().slideToggle(250);
        $('body, html').animate({
          scrollTop: $(this).offset().top
        }, 600);
      }
    });
  })();

  /* HTML5 placeholder, ie */
  $('input, textarea').placeholder();
});
