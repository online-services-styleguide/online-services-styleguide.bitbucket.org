(function() {
  if (document.getElementById('grid-control') === null) return;
  document.getElementById('grid-control').addEventListener('click', function() {
    if (document.body.classList.contains('grid-on')) {
      document.body.classList.remove('grid-on');
    } else {
      document.body.classList.add('grid-on');
    }
  });
})();


$(function() {
  // Colors
  (function() {
    if (! $('#color-template').length) return;
    var source = $('#color-template').html();
    var template = Handlebars.compile(source);
    var primaryColorPalette = {
      colors: [
        {
          name: 'Stargazer Blue',
          hex: '#003E6B',
          rgb: '0, 62, 107',
          scss: '$stargazer-blue'
        },
        {
          name: 'Prussian Blue',
          hex: '#002136',
          rgb: '0, 33, 54',
          scss: '$prussian-blue'
        },
        {
          name: 'Congress Blue',
          hex: '#065AA3',
          rgb: '6, 90, 163',
          scss: '$congress-blue'
        },
        {
          name: 'Electric Blue',
          hex: '#0075BC',
          rgb: '0, 117, 188',
          scss: '$electric-blue'
        },
        {
          name: 'Sky Blue',
          hex: '#5AA3D2',
          rgb: '90, 163, 210',
          scss: '$sky-blue'
        },
        {
          name: 'Baby Blue',
          hex: '#96D1F2',
          rgb: '150, 209, 242',
          scss: '$baby-blue'
        }
      ]
    };

    var grays  = {
      colors: [
        {
          name: 'White',
          hex: '#FFFFFF',
          rgb: '255, 255, 255',
          scss: '$white'
        },
        {
          name: 'Athens Gray',
          hex: '#EEF0F2',
          rgb: '238, 240, 242',
          scss: '$athens-gray'
        },
        {
          name: 'Silver Sand',
          hex: '#C4C7C9',
          rgb: '196, 199, 201',
          scss: '$silver-sand'
        },
        {
          name: 'Oslo Gray',
          hex: '#8A8F94',
          rgb: '138, 143, 148',
          scss: '$oslo-gray'
        },
        {
          name: 'Shuttle Gray',
          hex: '#5D6064',
          rgb: '93, 96, 100',
          scss: '$shuttle-gray'
        },
        {
          name: 'Bright Gray',
          hex: '#303440',
          rgb: '48, 52, 64',
          scss: '$bright-gray'
        },
        {
          name: 'Shark',
          hex: '#2E3032',
          rgb: '46, 48, 50',
          scss: '$shark'
        }
      ]
    };

    $('ul.primary-color-pallete').html(template(primaryColorPalette));
    $('ul.grays').html(template(grays));
  })();
});
